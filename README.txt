# README #

This document describes the steps undertaken as part of a skills assessment for a job application.

### Prerequisites  ###
* Windows VM (hosted in Azure)
* Chrome
* Visual studio community
* NuGet
* Install Selenium webdriver (via NuGet)

### Disclaimer ###
A template was used as the author hasn't written C# in over 16 months.
Git clone https://github.com/executeautomation/SeleniumWithSpecflow.git


## Review the test target/manually identify the steps requried ##


Vehicle type
#ph_pagebody_0_phthreecolumnmaincontent_0_panel_VehicleType_DDList

<<Select last option>>

Select
#ph_pagebody_0_phthreecolumnmaincontent_0_panel_PassengerVehicleSubType_DDList


Input address (Unit 7 11 Sample Street, Broadmeadows VIC 3047):
#ph_pagebody_0_phthreecolumnmaincontent_0_panel_AddressLine_SingleLine_CtrlHolderDivShown


Permit start date
-Use default


Select 1 day:
#ph_pagebody_0_phthreecolumnmaincontent_0_panel_PermitDuration_DDList

Click next:
#ph_pagebody_0_phthreecolumnmaincontent_0_panel_btnNext


Check for:
#main > div > p